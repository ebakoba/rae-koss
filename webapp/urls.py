

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^team$', views.team, name='team'),
    url(r'^sponsors$', views.sponsors, name='sponsors'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^youth$', views.youth, name='youth'),
    url(r'^est-fin-100$', views.schedule, name='schedule'),
]