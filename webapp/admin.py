from django.contrib import admin
from webapp.models import Team, Sponsor, Coach, Contact

admin.site.register(Team)
admin.site.register(Sponsor)
admin.site.register(Coach)
admin.site.register(Contact)
# Register your models here.
