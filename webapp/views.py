from django.shortcuts import render
from webapp.models import Team, Sponsor, Coach, Contact
from bs4 import BeautifulSoup
import urllib.request
import re

# Create your views here.
def index(request):
    return render(request, 'views/index.html', {'sponsors': Sponsor.objects.order_by('-priority').all(), 'active': 'index'})

def sponsors(request):
    return render(request, 'views/sponsors.html', {'sponsors': Sponsor.objects.order_by('-priority').all(), 'active': 'sponsors'})

def team(request):
    teams_list = []
    for team in Team.objects.all():
        url = urllib.request.urlopen(team.basket_url)
        soup = BeautifulSoup(url, "lxml")
        
        
        games_table = soup.find("table", {"class":"games_table"})
        if games_table:
            games_table['class'] = games_table.get('class', []) + ['mdl-data-table mdl-js-data-table']
            for tag in games_table.find_all('td', ["games_center_block games_result_box"]):
                print(tag)
                tag['onclick'] = "document.location='www.basket.ee" + tag.get('onclick').split("'")[1] + "'"
        

        players_table = soup.find("table", {"class":"basket_table"})
        if players_table:
            players_table['class'] = players_table.get('class', []) + ['mdl-data-table mdl-js-data-table']
            for tag in players_table.find_all('td', {"width":"165"}):
                tag.replaceWith('')
            for tag in players_table.find_all('th', text = re.compile('Periood')):
                tag.replaceWith('')

        teams_list.append({
            'name': team.name,
            'coach': team.coach,
            'picture_url': team.picture_url,
            'players_table': str(players_table),
            'games_table': str(games_table),
        })
    return render(request, 'views/team.html', {'teams': teams_list, 'active': 'team'})

def contact(request):
    return render(request, 'views/contact.html', {'contacts': Contact.objects.order_by('-priority').all(), 'active': 'contact'})

def youth(request):
    return render(request, 'views/youth.html', {'coaches': Coach.objects.all(), 'active': 'youth'})

def schedule(request):
    return render(request, 'views/schedule.html', {'active': 'schedule'})