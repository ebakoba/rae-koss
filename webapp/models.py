from django.db import models



class Team(models.Model):
    name = models.CharField(max_length = 200)
    coach = models.CharField(max_length = 200)
    picture_url = models.CharField(max_length = 400)
    basket_url = models.CharField(max_length = 200)

    def __str__(self):
        return self.name

class Sponsor(models.Model):
    name = models.CharField(max_length = 200)
    picture_url = models.CharField(max_length = 400)
    link = models.CharField(max_length = 200)
    text = models.TextField(null=True)
    priority = models.IntegerField(default=0)

    def __str__(self):
        return self.name

class Coach(models.Model):
    name = models.CharField(max_length = 200)
    age_group = models.CharField(max_length = 200)
    location = models.CharField(max_length = 200)

    def __str__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length = 200)
    profession = models.CharField(max_length = 200)
    email = models.EmailField()
    phone_number = models.CharField(max_length = 200)
    priority = models.IntegerField(default=0)

    def __str__(self):
        return self.name

# Create your models here.
